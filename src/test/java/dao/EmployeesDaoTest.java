package dao;

import sbp.school.hibernate.dao.EmployeesDao;
import sbp.school.hibernate.pojo.Employees;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class EmployeesDaoTest {

    SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
    EmployeesDao employeesDao = new EmployeesDao(sessionFactory);

    Employees employees1 = new Employees("Tom", "Johnson",
            LocalDate.of(1989, 11, 5), "IT");

    Employees employees2 = new Employees("Kate", "Farrel",
            LocalDate.of(1987, 12, 6), "HR");

    Employees employees3 = new Employees("Bill", "Thompson",
            LocalDate.of(1993, 01, 7), "SALE");

    @Test
    public void create_andFindAll_Test() {

        boolean result = employeesDao.create(employees1);
        Assert.assertEquals(true, result);

        result = employeesDao.create(employees2);
        Assert.assertEquals(true, result);

        List<Employees> list = employeesDao.findAll();

        Assert.assertEquals(list.get(0).getFirstName(), employees1.getFirstName());
        Assert.assertEquals(list.get(1).getBirthday(), employees2.getBirthday());

    }

    @Test
    public void update_andFindByName_Test() {

        String lastNameBeforeUpdate = employees3.getLastName();

        employees3.setLastName("Nickolas");

        employeesDao.create(employees3);

        employeesDao.update(employees3);

        List<Employees> result = employeesDao.findByName(employees3.getFirstName());

        Assert.assertEquals("Nickolas", result.get(0).getLastName());
        Assert.assertFalse(result.get(0).getLastName().equals(lastNameBeforeUpdate));

        System.out.println(result);

    }

    @Test
    public void deleteById_Test() {

        List<Employees> employees = employeesDao.findByName(employees3.getFirstName());

        employeesDao.deleteById(18);

        List<Employees> result = employeesDao.findByName(employees3.getFirstName());

        Assert.assertTrue(result.isEmpty());

    }

    @Test
    public void saveAllDevelopers() {
        List<Employees> employees = new ArrayList<>();
        employees.add(employees1);
        employees.add(employees2);
        employees.add(employees3);

       boolean result = employeesDao.saveAllDevelopers(employees);

       Assert.assertEquals(true, result);

    }

    @Test
    public void deleteAllDevelopers() {
        boolean bool = employeesDao.deleteAllDevelopers();
        List<Employees> list = employeesDao.findAll();

        Assert.assertTrue(list.isEmpty());
    }
}