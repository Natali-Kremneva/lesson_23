package sbp.school.hibernate.pojo;

import java.time.LocalDate;

public class Employees {

    private int id;
    private String firstName;
    private String lastName;
    private LocalDate birthday;
    private String department;

    public Employees(){

    }

    public Employees(String name, String lastName, LocalDate birthday, String department) {
        this.firstName = name;
        this.lastName = lastName;
        this.birthday = birthday;
        this.department = department;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String specialty) {
        this.department = specialty;
    }

    @Override
    public String toString() {
        return "Developer{" +
                "name='" + firstName + '\'' +
                ", lastname='" + lastName + '\'' +
                ", birthday=" + birthday +
                ", specialty='" + department + '\'' +
                '}';
    }
}
