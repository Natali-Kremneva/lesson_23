package sbp.school.hibernate.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import sbp.school.hibernate.pojo.Employees;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

public class EmployeesDao {

    private final SessionFactory sessionFactory;

    public EmployeesDao(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    public boolean create(Employees employees){

        Transaction transaction = null;

        try(Session session = this.sessionFactory.openSession()){
            transaction = session.beginTransaction();

            session.save(employees);

            transaction.commit();
            return true;

        }catch (Exception e){
            if(Objects.nonNull(transaction)) {
                transaction.rollback();
            }
            System.out.println("Insert employees info fail: " + e.getMessage());
            e.printStackTrace();
            return false;
        }
    }

    public List<Employees> findByName(String name) {

        Transaction transaction = null;

        List<Employees> employee = null;

        try (Session session = this.sessionFactory.openSession()) {
            transaction = session.beginTransaction();

            Query<Employees> query = session.createQuery("from Employees where firstName = :name", Employees.class);
            query.setParameter("name", name);
            employee = query.list();

            transaction.commit();

        } catch (Exception e) {
            if (Objects.nonNull(transaction)) {
                transaction.rollback();
            }
            System.out.println("Find employees by name info fail: " + e.getMessage());
            e.printStackTrace();

        }
        return employee;
    }

    public List<Employees> findAll() {

        Transaction transaction = null;

        List<Employees> allEmployees = new ArrayList<>();

        try (Session session = this.sessionFactory.openSession()) {
            transaction = session.beginTransaction();

            allEmployees = session.createQuery("from Employees").list();

            transaction.commit();

        } catch (Exception e) {
            if (Objects.nonNull(transaction)) {
                transaction.rollback();
            }
            System.out.println("Find all employees info fail: " + e.getMessage());
            e.printStackTrace();

        }
        return allEmployees;
    }

    public boolean update(Employees employee){

        Transaction transaction = null;

        try (Session session = this.sessionFactory.openSession()) {
            transaction = session.beginTransaction();

            session.update(employee);

            transaction.commit();
            return true;

        } catch (Exception e) {
            if (Objects.nonNull(transaction)) {
                transaction.rollback();
            }
            System.out.println("Update employees info fail: " + e.getMessage());
            e.printStackTrace();
            return false;
        }
    }

    public boolean deleteById(int id){

        Transaction transaction = null;

        try (Session session = this.sessionFactory.openSession()) {
            transaction = session.beginTransaction();

            Employees employee = session.get(Employees.class, id);

            session.delete(employee);

            transaction.commit();
            return true;

        } catch (Exception e) {
            if (Objects.nonNull(transaction)) {
                transaction.rollback();
            }
            System.out.println("Delete employees info fail: " + e.getMessage());
            e.printStackTrace();
            return false;
        }
    }

    public boolean saveAllDevelopers(Collection<Employees> employees) {

        Transaction transaction = null;

        try(Session session = this.sessionFactory.openSession()){
            transaction = session.beginTransaction();

            for (Employees employee : employees) {
                session.save(employee);
            }
            transaction.commit();
            return true;

        }catch (Exception e){
            if (Objects.nonNull(transaction)){
                transaction.rollback();
            }
            System.out.println("Save employees info fail: " + e.getMessage());
            e.printStackTrace();
            return false;
        }
    }

    public boolean deleteAllDevelopers() {

        Transaction transaction = null;

        try(Session session = this.sessionFactory.openSession()){
            transaction = session.beginTransaction();

            Query q = session.createQuery("delete from Employees");
            q.executeUpdate();

            transaction.commit();
            return true;

        }catch (Exception e){
            if (Objects.nonNull(transaction)){
                transaction.rollback();
            }
            System.out.println("Delete all employees info fail: " + e.getMessage());
            e.printStackTrace();
            return false;
        }
    }
}
